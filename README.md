![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo fornecer informações sobre o projeto Empresas.

### O QUE É ? ###

* O aplicativo Empresas foi criado para um teste do processo seletivo de estágio da ioasys.
* É um app que tem autenticação de usuário com OAuth2.0 e que, através de uma API, permite a busca de várias empresas diferentes.

### INSTRUÇÕES ###

* Para começar a utilizar o aplicativo, o usuário deve logar com a conta já criada para testes.
	Usuário: testeapple@ioasys.com.br
	Senha: 12341234
* Ao logar basta apenas o usuário clicar no ícone de busca no canto superior direito da tela para que uma nova tela de busca se abra.
* Na tela de busca, o usuário digita o nome da empresa que deseja visualizar, ou o nome da cidade ou do país em que a empresa está localizada. É possível também fazer a busca pelo tipo de empresa.
* Quando os resultados aparecem na tela, o usuário pode clicar em alguma para abrir uma nova tela contendo a descrição da empresa desejada.
* Para voltar à tela de busca, basta clicar no botão no canto superior esquerdo da tela.

### BIBLIOTECAS UTILIZADAS ###

* Retrofit: biblioteca utilizada para fazer requisições do tipo POST e GET tanto para o login do usuário, quanto para buscar as empresas através da API.
* Gson: biblioteca utilizada para fazer a conversão de objetos JSON.
* Glide: biblioteca utilizada para mostrar imagens através de uma URL.

### IDEIAS PARA IMPLEMENTAÇÕES FUTURAS ###

* Se tivesse mais tempo, eu colocaria um modo de favoritar empresas, para que elas apareçam na tela inicial.
* Colocaria mais modos de organizar os resultados da pesquisa, como país ou tipo de empresa.
* Em um cenário real, colocaria um botão na tela de detalhes da empresa para que o usuário possa se comunicar com a empresa, já que existem campos de redes sociais, telefone e e-mail para cada empresa.